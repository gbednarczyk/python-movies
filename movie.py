"""This module is for representing movies"""

class Movie(object):
    """This class represents a movie"""

    def __init__(self, genre, length):
        self.genre = genre
        self.length = length
